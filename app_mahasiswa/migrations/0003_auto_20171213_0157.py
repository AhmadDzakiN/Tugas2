# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 18:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_mahasiswa', '0002_auto_20171211_1101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pengguna',
            name='email',
            field=models.CharField(default='kosong', max_length=25, verbose_name='email'),
        ),
        migrations.AlterField(
            model_name='pengguna',
            name='foto_profil',
            field=models.URLField(default='https://hello-pet.com/assets/uploads/2016/03/aliando.jpg'),
        ),
        migrations.AlterField(
            model_name='pengguna',
            name='nama',
            field=models.CharField(default='kosong', max_length=25, verbose_name='nama'),
        ),
        migrations.AlterField(
            model_name='pengguna',
            name='npm',
            field=models.CharField(max_length=25, primary_key=True, serialize=False, unique=True, verbose_name='npm'),
        ),
        migrations.AlterField(
            model_name='pengguna',
            name='profile_linkedin',
            field=models.CharField(default='kosong', max_length=25, verbose_name='profile_linkedin'),
        ),
        migrations.AlterField(
            model_name='pengguna',
            name='username',
            field=models.CharField(default='kosong', max_length=25, unique=True, verbose_name='username'),
        ),
    ]
