from django.conf.urls import url
from .views import index
from .views_profile import *
from .views_riwayat import *
from .views_status import *
from .views_mahasiswa import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile', profile, name='profile'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^delete_status/(?P<id>\d+)/$', delete_status, name='delete_status'),
    
]