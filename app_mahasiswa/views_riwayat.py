from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
import requests
from.views import response

# bikin views.py buat masing" fitur di sini ya, nanti kalo mau dipake ke urls.py jangan lupa di import dulu filenya
response = {}
RIWAYAT_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def index(request):
    response['riwayats'] = get_riwayat()
    html = 'session/mahasiswaPage.html'
    response['rahasia'] = False
    return render(request, html, response)

def get_riwayat():
	riwayat = requests.get(RIWAYAT_API)
	return riwayat.json()