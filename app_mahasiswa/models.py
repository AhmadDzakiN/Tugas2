from django.db import models

# Create your models here.
class Pengguna(models.Model):
    nama = models.CharField('nama', max_length=25, default='kosong')
    username = models.CharField('username', max_length=25, default='kosong', unique=True)
    npm = models.CharField('npm', max_length=25, primary_key=True, unique=True)
    profile_linkedin = models.CharField('profile_linkedin', max_length=25, default='kosong')
    foto_profil = models.URLField(default='https://hello-pet.com/assets/uploads/2016/03/aliando.jpg')
    email = models.CharField('email', max_length=25, default='kosong')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Status(models.Model):
    status = models.TextField()
    pengguna = models.ForeignKey(Pengguna, null = True)
    created_date = models.DateTimeField(auto_now_add=True)

class Keahlian(models.Model):
    keahlian = models.CharField('keahlian', max_length = 10)
    level = models.CharField('level', max_length = 15)
    pengguna = models.ForeignKey(Pengguna)