from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
import requests
from .models import *
from .views import response

def profile(request):
    print ("#==> profile")

    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect('/login/')
    else:
        npm = request.session['npm']
        try:
            pengguna = Pengguna.objects.get(npm = npm)
            set_data_for_session(pengguna)
        except Exception as e:
            pengguna = Pengguna()
            pengguna.npm = npm     
            pengguna.username = request.session['user_login']       
            pengguna.save()
            set_data_for_session(pengguna)
        
        list_keahlian = get_my_keahlian_from_database(request)
        response['list_keahlian'] = list_keahlian

        html = 'session/mahasiswaPage.html'
        return render(request, html, response)

def set_data_for_sebession(user):
    response['username'] = user.username
    response['npm'] = user.npm
    response['foto_profil'] = user.foto_profil
    response['nama'] = user.nama
    response['profile_linkedin'] = user.profile_linkedin
    response['email'] = user.email

def get_my_keahlian_from_database(request):
    resp = []
    npm = request.session['npm']
    pengguna = Pengguna.objects.get(npm=npm)
    items = Keahlian.objects.filter(pengguna=pengguna)
    for item in items:
        resp.append(item)
    return resp 