[![pipeline status](https://gitlab.com/AhmadDzakiN/Tugas2/badges/master/pipeline.svg)](https://gitlab.com/AhmadDzakiN/Tugas2/commits/master)

[![coverage report](https://gitlab.com/AhmadDzakiN/Tugas2/badges/master/coverage.svg)](https://gitlab.com/AhmadDzakiN/Tugas2/commits/master)

# Nama Anggota Kelompok
1. Ahmad Dzaki Naufal
2. Reihan Putra Oktavio
3. Muhamad Arif Budiono
4. Mohammad Dwikurnia A.

# Link Herokuapp
Link: [https://tugasdua.herokuapp.com/](https://tugasdua.herokuapp.com/)